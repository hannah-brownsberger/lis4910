# Information Technology Project

## Hannah Brownsberger

### LIS4910 Course Description:
*Students work in teams or individually to manage, design, implement and evaluate an information technology project. Students are also given evaluation and guidance on improving artifacts from projects entered into their degree portfolio during this and other courses within the degree program.* 

### What I learned from LIS 4910:
 - how to create a gantt chart
 - how to use Figma to design prototypes and wireframes
 - how to develop an application from start to finish 

***Below are all of the components my team and I developed for this project over the course of the Fall 2021 semester: 3D Jewels.***

1. [Project Proposal](img/10.8.21_projectproposal.png)

2. [Work Breakdown Structure](img/10.11.21_workbreakdownstructure.png)

3. [Gantt Chart](img/ganttchart.png)

4. [Wireframes](https://bitbucket.org/hannah-brownsberger/lis4910/src/master/wireframes/)

5. [First Prototype](https://www.figma.com/file/DRKz9JglU3b0ZHX9pFVa2n/3D-Jewels-First-Prototype?node-id=0%3A1)

6. [Final Prototype](https://www.figma.com/file/YvznDYoJpVxw7aLne6nk2q/3D-Jewels-Final-Prototype)

7. [Communication Platform](https://hbrownsberger.wixsite.com/lis4910)

8. [Logo](img/logo.jpg)