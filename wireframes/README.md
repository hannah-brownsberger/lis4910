# Information Technology Project

## Hannah Brownsberger

### LIS4910 Project Wireframes:
*Here are the original proposed wireframes for our application.* 


| Home  | Customer Login | Company Login |
| ------------- | ------------- | ------------- |
| ![Wireframe Home](img/home.png) | ![Wireframe Customer Login](img/customerlogin.png)  | ![Wireframe Company Login](img/companylogin.png)  |

| 3D Platform  | Import File | Checkout |
| ------------- | ------------- | ------------- |
| ![Wireframe 3D Platform](img/3dplatform.png) | ![Wireframe Import File](img/importfile.png)  | ![Wireframe Checkout](img/checkout.png)  |
